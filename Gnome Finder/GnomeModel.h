//
//  GnomeModel.h
//  Gnome Finder
//
//  Created by Bruno Terra Porley on 15/09/15.
//  Copyright (c) 2015 Teddy Bear Studio. All rights reserved.
//

#import "JSONModel.h"
@protocol GnomeModel
@end

@interface GnomeModel : JSONModel

@property (assign, nonatomic) int gnomeId;
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSURL* profileImage;
@property (assign, nonatomic) int age;
@property (assign, nonatomic) float height;
@property (assign, nonatomic) float weight;
@property (strong, nonatomic) NSString* hairColor;
@property (strong, nonatomic) NSArray *professions;
@property (strong, nonatomic) NSArray *friends;
@end
