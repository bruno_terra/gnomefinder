//
//  GnomeListViewControllerTableViewController.m
//  Gnome Finder
//
//  Created by Bruno Terra Porley on 15/09/15.
//  Copyright (c) 2015 Teddy Bear Studio. All rights reserved.
//

#import "GnomeListViewController.h"
#import "GlobalVars.h"
#import "GnomeDataService.h"
#import "GnomeListTableViewCell.h"
#import "SearchGnomeTableViewController.h"
#import "FilterViewController.h"
#import "GnomeResumeViewController.h"
#import "GnomeModel.h"

@interface GnomeListViewController ()
    @property (nonatomic, weak) GlobalVars *globals;
    @property (nonatomic, strong) UISearchController *searchController;
    @property (nonatomic, strong) GnomeModel *selectedGnome;
@end

@implementation GnomeListViewController

static NSString *const gnomeCellIdentifier = @"gnomeCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.definesPresentationContext = YES;
    
    self.title = @"Village";
    if (_titleString) {
        self.title = _titleString;
    }
    
    //Setting the searchbar
    
    UINavigationController *searchResultsController = [[self storyboard] instantiateViewControllerWithIdentifier:@"TableSearchResultsNavigationController"];
    
    _searchController = [[UISearchController alloc] initWithSearchResultsController:searchResultsController];
    _searchController.searchResultsUpdater = self;
    
    [_searchController.searchBar sizeToFit];
    
    _gnomeTableView.tableHeaderView = _searchController.searchBar;
    
    //Setting the data
    _globals = [GlobalVars sharedInstance];
    
    if (_filterButton.isEnabled) {
        _filteredGnomes = [[NSMutableArray alloc]init];
        [GnomeDataService fetchDataWithSuccess:^{
            _filteredGnomes = _globals.gnomes;
            [_gnomeTableView reloadData];
        } andErrror:^(NSError *error) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No network connection"
                                                            message:@"You must be connected to the internet to use this app."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (_globals.ageFilterValueSelected != -1 || _globals.heightFilterValueSelected != -1 || _globals.weightFilterValueSelected != -1 || _globals.professionSelected.length > 0) {
        _filterButton.tintColor = [UIColor blueColor];
    } else {
        _filterButton.tintColor = [UIColor colorWithRed:111.0/255.0 green:113.0/255.0 blue:121.0/255.0 alpha:1];
    }
    
    if (_filterButton.isEnabled && _globals.gnomes.count > 0) {
        _filteredGnomes = [GlobalVars applyCurrentFilters];
        [_gnomeTableView reloadData];
    }
}

#pragma mark - Table View Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _filteredGnomes.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GnomeListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:gnomeCellIdentifier forIndexPath:indexPath];
    
    GnomeModel *gnome = [_filteredGnomes objectAtIndex:indexPath.row];
    
    cell.gnomeNameLabel.text = [gnome name];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _selectedGnome = _filteredGnomes[indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"gnomeSelectedSegue" sender:self];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"filterSegue"]) {
        UINavigationController *navController = segue.destinationViewController;
        FilterViewController *controller = (FilterViewController *)navController.topViewController;
        controller.filterApplied = ^void(NSMutableArray* filteredArray){
            _filteredGnomes = filteredArray;
            [_gnomeTableView reloadData];
        };
    } else if ([segue.identifier isEqualToString:@"gnomeSelectedSegue"]){
        UINavigationController *navController = segue.destinationViewController;
        GnomeResumeViewController *controller = (GnomeResumeViewController *)navController.topViewController;
        controller.selectedGnome = _selectedGnome;
    }
}


#pragma mark - Search Methods

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = _searchController.searchBar.text;
    
    if (_searchController.searchResultsController && ![_searchController.searchBar isHidden]) {
        
        UINavigationController *navController = (UINavigationController *) _searchController.searchResultsController;
        SearchGnomeTableViewController *vc = (SearchGnomeTableViewController *)navController.topViewController;
        vc.searchController = _searchController;
        vc.searchResults = [self updateFilteredContentForGnomeName:searchString];
        
        [vc.tableView reloadData];
    }
}

- (NSMutableArray *)updateFilteredContentForGnomeName:(NSString *)gnomeName
{
    if (gnomeName == nil) {
        return [_filteredGnomes mutableCopy];
    } else {
        
        NSMutableArray *searchResults = [[NSMutableArray alloc] init];
        
        for (GnomeModel *gnome in _filteredGnomes) {
            NSRange range = [gnome.name  rangeOfString: gnomeName options: NSCaseInsensitiveSearch];
            if (range.location != NSNotFound) {
                [searchResults addObject:gnome];
            }
        }
        
        return searchResults;
    }
}

#pragma mark - IBActions

- (IBAction)filterPressed:(id)sender {
    [self performSegueWithIdentifier:@"filterSegue" sender:self];
}


@end
