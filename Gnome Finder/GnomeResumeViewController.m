//
//  GnomeResumeViewController.m
//  Gnome Finder
//
//  Created by Bruno Terra Porley on 16/09/15.
//  Copyright (c) 2015 Teddy Bear Studio. All rights reserved.
//

#import "GnomeResumeViewController.h"
#import "ProfessionDetailTableViewCell.h"
#import "FriendDetailTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface GnomeResumeViewController ()
@property (weak, nonatomic) IBOutlet UITableView *professionTableView;
@property (weak, nonatomic) IBOutlet UITableView *friendTableView;
@property (weak, nonatomic) IBOutlet UIImageView *profileBackground;
@property (weak, nonatomic) IBOutlet UIImageView *profilePicture;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *heightLabel;
@property (weak, nonatomic) IBOutlet UILabel *weightLabel;
@property (weak, nonatomic) IBOutlet UILabel *hairColorLabel;
@property (weak, nonatomic) IBOutlet UIImageView *noFriendsImage;
@property (weak, nonatomic) IBOutlet UIImageView *noWorkImage;

@end

@implementation GnomeResumeViewController
static NSString *const professionCellIdentifier = @"professionCell";
static NSString *const friendCellIdentifier = @"friendCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setInitialContent];
    self.title = @"Gnome";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

-(void)setInitialContent{
    
    [_profilePicture sd_setImageWithURL:_selectedGnome.profileImage placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [_profilePicture layoutIfNeeded];
    _profilePicture.layer.cornerRadius = _profilePicture.frame.size.width / 2;
    _profilePicture.clipsToBounds = YES;
    _profilePicture.contentMode = UIViewContentModeScaleAspectFill;
    
    [_profileBackground sd_setImageWithURL:_selectedGnome.profileImage placeholderImage:[UIImage imageNamed:@"placeholder"]];
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
    effectView.frame = self.view.frame;
    [_profileBackground addSubview:effectView];
    
    
    _nameLabel.text = _selectedGnome.name;
    _hairColorLabel.text = [NSString stringWithFormat:@"Hair C: %@", _selectedGnome.hairColor];
    
    _ageLabel.text = [NSString stringWithFormat:@"Age: %i",_selectedGnome.age];
    _heightLabel.text = [NSString stringWithFormat:@"Height: %i",(int)(_selectedGnome.height + 0.5)];
    _weightLabel.text = [NSString stringWithFormat:@"Weight: %i",(int)(_selectedGnome.weight + 0.5)];
    

}


#pragma mark - Table View Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _professionTableView) {
        NSInteger val = (_selectedGnome.professions.count + 1) / 2;
        if (val == 0) {
            _noWorkImage.hidden = NO;
        } else {
            _noWorkImage.hidden = YES;
        }
        return val;
    } else if (tableView == _friendTableView){
        NSInteger val = (_selectedGnome.friends.count + 1) / 2;
        if (val == 0) {
            _noFriendsImage.hidden = NO;
        } else {
            _noFriendsImage.hidden = YES;
        }

        return val;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger dataStartingRow = indexPath.row * 2;
    
    if (tableView == _professionTableView) {
        ProfessionDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:professionCellIdentifier forIndexPath:indexPath];
        cell.firstProfessionLabel.text = _selectedGnome.professions[dataStartingRow];
        if (_selectedGnome.professions.count > dataStartingRow + 1) {
            cell.secondProfessionLabel.text = _selectedGnome.professions[dataStartingRow + 1];
        } else {
            cell.secondProfessionLabel.text = @"";
        }
        return cell;
        
    } else if (tableView == _friendTableView){
        FriendDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:friendCellIdentifier forIndexPath:indexPath];
        cell.firstFriendLabel.text = _selectedGnome.friends[dataStartingRow];
        if (_selectedGnome.friends.count > dataStartingRow + 1) {
            cell.secondFriendLabel.text = _selectedGnome.friends[dataStartingRow + 1];
        } else {
            cell.secondFriendLabel.text = @"";
        }
        return cell;
        
    }
    
    return nil;
}

@end
