//
//  GnomeDataService.m
//  Gnome Finder
//
//  Created by Bruno Terra Porley on 15/09/15.
//  Copyright (c) 2015 Teddy Bear Studio. All rights reserved.
//

#import "GnomeDataService.h"
#import "AFNetworking.h"
#import "GlobalVars.h"

@implementation GnomeDataService

+(void)fetchDataWithSuccess:(void (^)()) success andErrror: (void (^)(NSError* error)) failure {
    GlobalVars *globals = [GlobalVars sharedInstance];
    
    NSURL *URL = [NSURL URLWithString:@"https://raw.githubusercontent.com/AXA-GROUP-SOLUTIONS/mobilefactory-test/master/data.json"];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    op.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
    
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        TownModel *town = [[TownModel alloc] initWithDictionary:responseObject error:nil];
        
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
        NSArray *sortedArray = [town.gnomes sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
        
        globals.gnomes = [[NSMutableArray alloc] initWithArray: sortedArray  ];
        
        for (GnomeModel *gnome in town.gnomes) {
            for (NSString *prof in gnome.professions) {
                if (![globals.professions containsObject:prof]) {
                    [globals.professions addObject:prof];
                }
            }
        }
        
        NSLog(@"JSON: %@", responseObject);
        success();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        failure(error);
    }];
    [[NSOperationQueue mainQueue] addOperation:op];
}

@end
