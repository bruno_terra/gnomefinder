//
//  FilterViewController.m
//  Gnome Finder
//
//  Created by Bruno Terra Porley on 15/09/15.
//  Copyright (c) 2015 Teddy Bear Studio. All rights reserved.
//

#import "FilterViewController.h"
#import "GlobalVars.h"
#import "GnomeModel.h"

#define NUMBERS_ONLY @"1234567890"
#define CHARACTER_LIMIT 5

@interface FilterViewController (){
    NSArray *pickerOptions;
}
@property (weak, nonatomic) IBOutlet UITextField *ageValueField;
@property (weak, nonatomic) IBOutlet UITextField *weightValueField;
@property (weak, nonatomic) IBOutlet UITextField *heightValueField;

@property (weak, nonatomic) IBOutlet UIPickerView *agePicker;
@property (weak, nonatomic) IBOutlet UIPickerView *heightPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *weightPicker;
@property (weak, nonatomic) IBOutlet UILabel *professionLabel;
@property (weak, nonatomic) IBOutlet UIView *professionView;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;

@end

@implementation FilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Initialize Data
    pickerOptions = @[@"<", @"=", @">"];
    [self setInitialValues];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Private Methods

-(void)setInitialValues {
    
    _backgroundView.layer.cornerRadius = 5;
    _backgroundView.layer.masksToBounds = YES;
    
    _professionView.layer.cornerRadius = 5;
    _professionView.layer.masksToBounds = YES;
    
    GlobalVars *global = [GlobalVars sharedInstance];
    
    if (global.ageFilterTypeSelected != -1) {
        [_agePicker selectRow:global.ageFilterTypeSelected inComponent:0 animated:YES];
        [_agePicker reloadComponent:0];
    }
    
    if (global.heightFilterTypeSelected != -1) {
        [_heightPicker selectRow:global.heightFilterTypeSelected inComponent:0 animated:YES];
        [_heightPicker reloadComponent:0];
    }
    
    if (global.weightFilterTypeSelected != -1) {
        [_weightPicker selectRow:global.weightFilterTypeSelected inComponent:0 animated:YES];
        [_weightPicker reloadComponent:0];
    }
    
    if (global.ageFilterValueSelected != -1) {
        _ageValueField.text = [NSString stringWithFormat: @"%ld", (long)global.ageFilterValueSelected];
    }
    
    if (global.heightFilterValueSelected != -1) {
        _heightValueField.text = [NSString stringWithFormat: @"%ld", (long)global.heightFilterValueSelected];
    }
    
    if (global.weightFilterValueSelected != -1) {
        _weightValueField.text = [NSString stringWithFormat: @"%ld", (long)global.weightFilterValueSelected];
    }
    
    if (global.professionSelected.length > 0) {
        _professionView.hidden = NO;
        _professionLabel.text = [NSString stringWithFormat:@"Profession: %@",global.professionSelected];
    }

}

#pragma mark - Pickers Methods 

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerOptions.count;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return pickerOptions[row];
}

#pragma mark - IBActions

- (IBAction)clearContent:(id)sender {
    _ageValueField.text = @"";
    _weightValueField.text = @"";
    _heightValueField.text = @"";
    _professionView.hidden = YES;
}

- (IBAction)closeWindow:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)applyFilters:(id)sender {
    NSInteger agePickerSelectedValue = [_agePicker selectedRowInComponent:0];
    NSInteger heightPickerSelectedValue = [_heightPicker selectedRowInComponent:0];
    NSInteger weightPickerSelectedValue = [_weightPicker selectedRowInComponent:0];
    
    NSInteger ageValue = -1;
    NSInteger heightValue = -1;
    NSInteger weightValue = -1;
    
    if (_ageValueField.text.length > 0) {
        ageValue = [_ageValueField.text integerValue];
    }
    
    if (_heightValueField.text.length > 0) {
        heightValue = [_heightValueField.text integerValue];
    }
    
    if (_weightValueField.text.length > 0) {
        weightValue = [_weightValueField.text integerValue];
    }
    
    GlobalVars *global = [GlobalVars sharedInstance];
    
    global.ageFilterTypeSelected = agePickerSelectedValue;
    global.heightFilterTypeSelected = heightPickerSelectedValue;
    global.weightFilterTypeSelected = weightPickerSelectedValue;
    
    global.ageFilterValueSelected = ageValue;
    global.heightFilterValueSelected = heightValue;
    global.weightFilterValueSelected = weightValue;
    
    if (_professionView.isHidden) {
        global.professionSelected = @"";
    }
    
    _filterApplied([GlobalVars applyCurrentFilters]);
    [self closeWindow:self];
}


#pragma mark - UITextField Methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS_ONLY] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return (([string isEqualToString:filtered])&&(newLength <= CHARACTER_LIMIT));
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
