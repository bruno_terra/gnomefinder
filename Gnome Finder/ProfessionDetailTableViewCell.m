//
//  ProfessionDetailTableViewCell.m
//  Gnome Finder
//
//  Created by Bruno Terra Porley on 16/09/15.
//  Copyright (c) 2015 Teddy Bear Studio. All rights reserved.
//

#import "ProfessionDetailTableViewCell.h"

@implementation ProfessionDetailTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
