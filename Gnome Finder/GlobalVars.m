//
//  GlobalVars.m
//  Gnome Finder
//
//  Created by Bruno Terra Porley on 15/09/15.
//  Copyright (c) 2015 Teddy Bear Studio. All rights reserved.
//

#import "GlobalVars.h"
#import "GnomeModel.h"

@implementation GlobalVars

+ (GlobalVars *)sharedInstance {
    static dispatch_once_t onceToken;
    static GlobalVars *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[GlobalVars alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        _gnomes = [[NSMutableArray alloc] init];
        _professions = [[NSMutableArray alloc] init];
        _ageFilterTypeSelected = -1;
        _weightFilterTypeSelected = -1;
        _heightFilterTypeSelected = -1;
        _weightFilterValueSelected = -1;
        _heightFilterValueSelected = -1;
        _ageFilterValueSelected = -1;
        _professionSelected = @"";
    }
    return self;
}

+(NSMutableArray *)applyCurrentFilters{
    NSMutableArray *filteredValues = [[NSMutableArray alloc]init];
    
    GlobalVars *global = [GlobalVars sharedInstance];
    
    for (GnomeModel *gnome in global.gnomes) {
        BOOL validAge = YES;
        BOOL validHeight= YES;
        BOOL validWeight = YES;
        BOOL validProfession = YES;
        
        if (global.ageFilterValueSelected != -1) {
            switch (global.ageFilterTypeSelected) {
                case 0:
                    if (gnome.age < global.ageFilterValueSelected) {
                        validAge = YES;
                    }else {
                        validAge = NO;
                    }
                    break;
                case 1:
                    if (gnome.age == global.ageFilterValueSelected) {
                        validAge = YES;
                    }else {
                        validAge = NO;
                    }
                    break;
                case 2:
                    if (gnome.age > global.ageFilterValueSelected) {
                        validAge = YES;
                    }else {
                        validAge = NO;
                    }
                    break;
                default:
                    validAge = NO;
                    break;
            }
        }
        
        if (global.heightFilterValueSelected != -1) {
            switch (global.heightFilterTypeSelected) {
                case 0:
                    if (gnome.height < global.heightFilterValueSelected) {
                        validHeight = YES;
                    }else {
                        validHeight = NO;
                    }
                    break;
                case 1:
                    if (gnome.height == global.heightFilterValueSelected) {
                        validHeight = YES;
                    }else {
                        validHeight = NO;
                    }
                    break;
                case 2:
                    if (gnome.height > global.heightFilterValueSelected) {
                        validHeight = YES;
                    }else {
                        validHeight = NO;
                    }
                    break;
                default:
                    validHeight = NO;
                    break;
            }
        }
        
        if (global.weightFilterValueSelected != -1) {
            switch (global.weightFilterTypeSelected) {
                case 0:
                    if (gnome.weight < global.weightFilterValueSelected) {
                        validWeight = YES;
                    }else {
                        validWeight = NO;
                    }
                    break;
                case 1:
                    if (gnome.weight == global.weightFilterValueSelected) {
                        validWeight = YES;
                    }else {
                        validWeight = NO;
                    }
                    break;
                case 2:
                    if (gnome.weight > global.weightFilterValueSelected) {
                        validWeight = YES;
                    }else {
                        validWeight = NO;
                    }
                    break;
                default:
                    validWeight = NO;
                    break;
            }
        }
        
        if (global.professionSelected.length > 0 && ![gnome.professions containsObject:global.professionSelected]) {
            validProfession = NO;
        }
        
        if (validAge && validHeight && validWeight && validProfession) {
            [filteredValues addObject:gnome];
        }
    }
    return filteredValues;
}

@end
