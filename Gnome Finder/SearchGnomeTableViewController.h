//
//  SearchGnomeTableViewController.h
//  Gnome Finder
//
//  Created by Bruno Terra Porley on 15/09/15.
//  Copyright (c) 2015 Teddy Bear Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchGnomeTableViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic, weak) UISearchController *searchController;

@end
