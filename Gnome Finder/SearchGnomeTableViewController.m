//
//  SearchGnomeTableViewController.m
//  Gnome Finder
//
//  Created by Bruno Terra Porley on 15/09/15.
//  Copyright (c) 2015 Teddy Bear Studio. All rights reserved.
//

#import "SearchGnomeTableViewController.h"
#import "GnomeModel.h"
#import "GnomeResumeViewController.h"

@interface SearchGnomeTableViewController ()

@property (nonatomic, strong) GnomeModel *selectedGnome;

@end

@implementation SearchGnomeTableViewController

static NSString *const searchCellIdentifier = @"searchCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Search";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (_searchController.searchBar.text.length > 0) {
        _searchController.searchBar.hidden = NO;
    }
}

#pragma mark - Table view Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _searchResults.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:searchCellIdentifier forIndexPath:indexPath];
    GnomeModel *gnome = (GnomeModel *) _searchResults[indexPath.row];
    cell.textLabel.text = gnome.name;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _selectedGnome = _searchResults[indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"gnomeSelectedSegue" sender:self];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"gnomeSelectedSegue"]){
        UINavigationController *navController = segue.destinationViewController;
        GnomeResumeViewController *controller = (GnomeResumeViewController *)navController.topViewController;
        controller.selectedGnome = _selectedGnome;
        _searchController.searchBar.hidden = YES;
    }

}


@end
