//
//  ProfessionDetailTableViewCell.h
//  Gnome Finder
//
//  Created by Bruno Terra Porley on 16/09/15.
//  Copyright (c) 2015 Teddy Bear Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfessionDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *firstProfessionLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondProfessionLabel;

@end
