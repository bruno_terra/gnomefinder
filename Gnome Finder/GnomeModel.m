//
//  GnomeModel.m
//  Gnome Finder
//
//  Created by Bruno Terra Porley on 15/09/15.
//  Copyright (c) 2015 Teddy Bear Studio. All rights reserved.
//

#import "GnomeModel.h"

@implementation GnomeModel

+ (JSONKeyMapper*)keyMapper {
    NSDictionary *map = @{ @"id": @"gnomeId",
                           @"thumbnail" : @"profileImage",
                           @"hair_color" : @"hairColor"};
    
    return [[JSONKeyMapper alloc] initWithDictionary:map];
}

@end
