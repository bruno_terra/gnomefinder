//
//  TownModel.m
//  Gnome Finder
//
//  Created by Bruno Terra Porley on 15/09/15.
//  Copyright (c) 2015 Teddy Bear Studio. All rights reserved.
//

#import "TownModel.h"

@implementation TownModel

+ (JSONKeyMapper*)keyMapper {
    NSDictionary *map = @{ @"Brastlewark": @"gnomes"};
    
    return [[JSONKeyMapper alloc] initWithDictionary:map];
}

@end
