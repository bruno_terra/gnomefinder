//
//  ProffesionPickerViewController.m
//  Gnome Finder
//
//  Created by Bruno Terra Porley on 15/09/15.
//  Copyright (c) 2015 Teddy Bear Studio. All rights reserved.
//

#import "ProffesionPickerViewController.h"
#import "ProfessionTableViewCell.h"
#import "GlobalVars.h"
#import "GnomeModel.h"
#import "GnomeListViewController.h"

@interface ProffesionPickerViewController ()

@property (nonatomic, weak) GlobalVars *globals;

@end

@implementation ProffesionPickerViewController

NSString *professionSelected;

static NSString *const professionCellIdentifier = @"professionCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Professions";
    _globals = [GlobalVars sharedInstance];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _globals.professions.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProfessionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:professionCellIdentifier forIndexPath:indexPath];
    
    
    cell.professionLabel.text = [_globals.professions objectAtIndex:indexPath.row];
    cell.backgroundProfessionImage.image = [self randomImageForProfession];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    professionSelected = _globals.professions[indexPath.row];
    _globals.professionSelected = _globals.professions[indexPath.row];;
    NSLog(@"Selected Profession: %@", professionSelected);
    
    [self performSegueWithIdentifier:@"professionSegue" sender:self];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"professionSegue"]) {
        ((GnomeListViewController *)segue.destinationViewController).filteredGnomes = [self getGnomesByProfession];
        ((GnomeListViewController *)segue.destinationViewController).filterButton.enabled = NO;
        ((GnomeListViewController *)segue.destinationViewController).titleString = professionSelected;
    }
}

#pragma mark - private
-(NSMutableArray *)getGnomesByProfession{
    NSMutableArray *response = [[NSMutableArray alloc] init];
    for (GnomeModel *gnome in _globals.gnomes) {
        if ([gnome.professions containsObject:professionSelected]) {
            [response addObject:gnome];
        }
    }
    return response;
}


-(UIImage *)randomImageForProfession{
    int r = arc4random_uniform(13);
    
    switch (r) {
        case 0:
            return [UIImage imageNamed:@"baker"];
            break;
        case 1:
            return [UIImage imageNamed:@"brewer"];
            break;
        case 2:
            return [UIImage imageNamed:@"carpenter"];
            break;
        case 3:
            return [UIImage imageNamed:@"farmer"];
            break;
        case 4:
            return [UIImage imageNamed:@"food"];
            break;
        case 5:
            return [UIImage imageNamed:@"gold"];
            break;
        case 6:
            return [UIImage imageNamed:@"leatherworker"];
            break;
        case 7:
            return [UIImage imageNamed:@"mechanic"];
            break;
        case 8:
            return [UIImage imageNamed:@"mine"];
            break;
        case 9:
            return [UIImage imageNamed:@"pottery"];
            break;
        case 10:
            return [UIImage imageNamed:@"sculpture"];
            break;
        case 11:
            return [UIImage imageNamed:@"smelter"];
            break;
        case 12:
            return [UIImage imageNamed:@"stones"];
            break;
        case 13:
            return [UIImage imageNamed:@"woods"];
            break;
        default:
            return [UIImage imageNamed:@"baker"];
            break;
    }
}

@end
