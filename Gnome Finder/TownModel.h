//
//  TownModel.h
//  Gnome Finder
//
//  Created by Bruno Terra Porley on 15/09/15.
//  Copyright (c) 2015 Teddy Bear Studio. All rights reserved.
//

#import "JSONModel.h"
#import "GnomeModel.h"

@interface TownModel : JSONModel

@property (strong, nonatomic) NSArray<GnomeModel> *gnomes;

@end
