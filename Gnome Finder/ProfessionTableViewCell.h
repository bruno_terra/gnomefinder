//
//  ProfessionTableViewCell.h
//  Gnome Finder
//
//  Created by Bruno Terra Porley on 15/09/15.
//  Copyright (c) 2015 Teddy Bear Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfessionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *backgroundProfessionImage;
@property (weak, nonatomic) IBOutlet UILabel *professionLabel;

@end
