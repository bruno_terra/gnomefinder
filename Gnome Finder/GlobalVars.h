//
//  GlobalVars.h
//  Gnome Finder
//
//  Created by Bruno Terra Porley on 15/09/15.
//  Copyright (c) 2015 Teddy Bear Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobalVars : NSObject

+ (GlobalVars *)sharedInstance;

@property(strong, nonatomic, readwrite) NSMutableArray *gnomes;
@property(strong, nonatomic, readwrite) NSMutableArray *professions;
@property(strong, nonatomic, readwrite) NSString *professionSelected;

@property(nonatomic, readwrite) NSInteger ageFilterValueSelected;
@property(nonatomic, readwrite) NSInteger weightFilterValueSelected;
@property(nonatomic, readwrite) NSInteger heightFilterValueSelected;

@property(nonatomic, readwrite) NSInteger ageFilterTypeSelected;
@property(nonatomic, readwrite) NSInteger weightFilterTypeSelected;
@property(nonatomic, readwrite) NSInteger heightFilterTypeSelected;

+(NSMutableArray *)applyCurrentFilters;

@end