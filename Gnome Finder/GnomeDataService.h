//
//  GnomeDataService.h
//  Gnome Finder
//
//  Created by Bruno Terra Porley on 15/09/15.
//  Copyright (c) 2015 Teddy Bear Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TownModel.h"


@interface GnomeDataService : NSObject

+(void)fetchDataWithSuccess:(void (^)()) success andErrror: (void (^)(NSError* error)) failure;

@end
