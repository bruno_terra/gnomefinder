//
//  GnomeResumeViewController.h
//  Gnome Finder
//
//  Created by Bruno Terra Porley on 16/09/15.
//  Copyright (c) 2015 Teddy Bear Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GnomeModel.h"

@interface GnomeResumeViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) GnomeModel *selectedGnome;

@end
